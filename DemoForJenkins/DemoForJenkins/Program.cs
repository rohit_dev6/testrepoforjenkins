﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoForJenkins
{
    public class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Build Success");
            MainFunction();
        }
        public static void MainFunction()
        {
            int counter = 0;
            string line;

            // Read the file and display it line by line.  
            System.IO.StreamReader file =
                new System.IO.StreamReader(@"C:\Users\Anjali Tawde\Desktop\OutputJSON.json");
            while ((line = file.ReadLine()) != null)
            {
                System.Console.WriteLine(line);
                counter++;
            }

            file.Close();
            System.Console.WriteLine("There were {0} lines.", counter);
            // Suspend the screen.   
        }
    }
}
